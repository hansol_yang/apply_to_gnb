class CreateApplicants < ActiveRecord::Migration
  def change
    create_table :applicants do |t|
      t.integer :studentNumber
      t.string :applicantName
      t.string :phoneNumber
      t.timestamps null: false
    end
  end
end
