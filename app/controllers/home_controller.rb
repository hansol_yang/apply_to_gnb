class HomeController < ApplicationController
  def index
  end

  def apply
    applicant = Applicant.find_by studentNumber: params[:studentNumber]
    if(applicant)
      flash[:danger] = '해당 학번으로 이미 신청하셨습니다.'
      redirect_to '/'
    else
      newApply = Applicant.new
      newApply.studentNumber = params[:studentNumber]
      newApply.applicantName = params[:applicantName]
      newApply.phoneNumber = params[:phoneNumber]
      newApply.save
      flash[:success] = params[:applicantName] + '님의 지원이 정상적으로 처리되었습니다.'
      redirect_to '/'
    end
  end

  def applicantList
    @applicants = Applicant.all
  end
end
