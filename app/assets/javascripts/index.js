$(function() {
    $('.scrollTop').click(function() {
        $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 1000);
        return false;
    })

    setTimeout(function() {
        $('.alert').hide('slow');
    }, 2500);
})

function _onChange(e) {
    var value = e.target.value;

    if(isNaN(value)) {
        alert("숫자만 입력해주세요.");
        e.target.value = '';
    }
}